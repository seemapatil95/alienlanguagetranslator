package com.translate.implementation;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AlienLanguageTranslatorFromDefaultLanguageTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		AlienLanguageTranslator alt = new AlienLanguageTranslator();
		String o = alt.fromDefaultLanguage("java");
		assertEquals("mdyd", "mdyd");
	}

}
