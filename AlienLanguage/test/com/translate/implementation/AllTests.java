package com.translate.implementation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ AlienLanguageTranslatorFromDefaultLanguageTest.class,
		AlienLanguageTranslatorToDefaultLanguageTest.class })
public class AllTests {

}
